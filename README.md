# Dropbox Tag API

 This project is a Rest API that extends the Dropbox API functionality by introducing "tags".
 
* [HOW TO RUN THE APPLICATION](#why)
* [HOW PASS ENVIRONMENT VARIABLES](#why)
* [AUTHENTICATION](#authentication)
* [API DETAIL](#apidetail)
* [ERROR HANDLING](#errorHandling)
* [CODE](#code)

     
## HOW TO RUN THE APPLICATION?


* There are two applications which are 
    -   REST API Server (Spring boot)
    -   Single Node Solr Instance
* At the source code folder, there is a "build" folder which inludes three files.
    - app-run.sh : 
        - This file builds java project with maven and generates the docker image file. 
                    
                    mvn clean
                    mvn package docker:build
        
        - Then, it runs docker images
           
                docker run -p 8080:8080 -t mobilabsolutions/dropboxtagapi
           
    - solr-run.sh :
        - This file builds a solr instance in docker.
        
                docker run --name docker_solr_1 -d -p 8983:8983 -t solr 
            
        - Creates solr core whose name is "tags"
        
                docker exec -it --user=solr docker_solr_1 solr create_core -c tags
        
        - Then, copy schema file to docker container solr path
        
                docker cp managed-schema  docker_solr_1:/opt/solr/server/solr/tags/conf/managed-schema
        
        - Then, restart the docker container.
        
                docker stop docker_solr_1
                docker start docker_solr_1
       
    - managed-schema :
        - This file stores the data schema that our web server needs. Therefore, this file is going
         to be copied to docker instance so that we could not add field on solr ui interface.
         
* To run application:
    * Firstly, we need to open terminal under "build" folder (at the source code folder)
    * Then, we need to start Solr instance
        
            ./solr-run.sh
        
        - This command takes time because it will download the solr files.
        - After this command is finished, you should access Solr interface on 
        http://localhost:8983/solr
        - Then, you should select the "tag" core
        - Then, you should select Schema on left menu.
        - Then, you will see 5 fields that server uses which are 

        
                    <field name="id" type="string" multiValued="false" indexed="true" required="true" stored="true"/>
                    <field name="tagName" type="text_general" multiValued="false" indexed="true" required="true" stored="true"/>
                    <field name="filePath" type="text_general" multiValued="false" indexed="true" required="true" stored="true"/>
                    <field name="fileName" type="text_general" multiValued="false" indexed="true" required="true" stored="true"/>
                    <field name="userId" type="text_general" multiValued="false" indexed="true" required="true" stored="true"/>
                
                
   * Then, we need to start to rest api server.
        
           ./app-run.sh
        
     - This command will get the builds and then run application at docker.
     - After the command is finished, you should access the api welcome screen :) http://localhost:8080/api/1/


## HOW PASS ENVIRONMENT VARIABLES 
*  There are 4 variables that you can control with java environmental variables.
    These are 
    
        - solr.url : 
        - dropbox.api.app.key : Dropbox App Key
        - dropbox.api.app.secret : Dropbox App Secret
        - dropbox.api.redirect.url : Dropbox Redirect URl after user confirmation 
        
      
* You can run docker image like this :

          docker run -e JAVA_OPTS='-Dsolr.url=http://localhost:8080/solr/tags22222 -Ddropbox.api.app.key=asdasdad -Ddropbox.api.app.secret=dasda -Ddropbox.api.redirect.url=http://localhost:8080/api/1/auth/token/' -p 8080:8080 -t mobilabsolutions/dropboxtagapi

* These are default valuse :

        solr.url=http://localhost:8983/solr/tags
        dropbox.api.app.key=g5tqa8zsw5lz6ab
        dropbox.api.app.secret=88wem7gt5rwceap
        dropbox.api.redirect.url=http://localhost:8080/api/1/auth/token/


## AUTHENTICATION

* In order to get Dropbox Access Token, you should open web browser and open the http://localhost:8080/api/1/auth. Then it redirects you to dropbox login page.
 After you click "Allow" button, then it redirects you http://localhost:8080/api/1/auth/token which gives you to Dropbox Access Token.
 
 After getting this token, you should also give this token to each api request header, otherwise, you shouldn't make api call.


  **You should add redirect uri in Dropbox App settings.** 
 
![Alt text](src/main/resources/readmeimages/1_dropbox_rediredt_uri_settings.png?raw=true "Optional Title")
 
  **Open browser**
 
 ![Alt text](src/main/resources/readmeimages/2_authentication_start_point.png?raw=true "Optional Title")
  
   
 **Dropbox confirmation page**
 
 ![Alt text](src/main/resources/readmeimages/3_dropbox_confirmation.png?raw=true "Optional Title")
  
  **Dropbox access token  page**
 ![Alt text](src/main/resources/readmeimages/4_access_token_result.png?raw=true "Optional Title")
  
 
## API DETAIL

* In general API consists of 7 endpoints.
 
* GET : /api/1/auth/ 
    This rest end point just redirects user to dropbox confirmation page in order to get access token, in other words, our application needs to be registered to Dropbox API by giving api key and api secret key so that it can send requests to Dropbox. 
    This request should be made at web browser.

* GET : /api/1/auth/token
    This rest end point handles the request that Dropbox confirmation pages redirects and it gets the request from Dropbox and form the access token for each user.
    It returns json object which includes the access token. This request shouldn't be called at rest clients such as POSTMAN because it is called by Dropbox API.
        
       {
               "accessToken":"xxxxxxxxxxxxxxxxx",
               "userId":"xxxxxxx",
               "urlState":null
        }

* POST : /api/1/files/listFolder/

    This rest end point is kind of a transition between our user and dropbox api. When user calls this end point, our server calls the Dropbox "files/listFolder" api 
    with same request body therefore it proves that our rest api can access the dropbox api.
    
     Header:

            Content-Type : application/json 
            X-Auth-Access-Token : xxxxxxxxxxx 
     
    Request Body :
    
            {
               "path":"xxxx "
            }
            
    Response Body:
    
            [
                {
                    "tag": "folder",
                    "path": "/photos",
                    "name": "Photos"
                },
                {
                    "tag": "folder",
                    "path": "/format",
                    "name": "format"
                },
            ]


* POST : /api/1/tag
    
    This rest api enables user to add multiple tags to specific file. Then api returns all tags with tag id.
    
    Header:
    
        Content-Type : application/json
        X-Auth-Access-Token : xxxxxxxxxxx
    
    Request Body : 
        
         {
            "filePath": "/mertagtas",
            "fileName": "mertagtas.docx",
            "tagList": [
              "cool",
              "stuff"
            ]
          }
    
    Response Body : 
    
    
        [
            {
                "id": "25ed2498-a010-4ad6-aa9a-657cb7f58430",
                "tagName": "cool",
                "filePath": "/mertagtas",
                "fileName": "mertagtas.docx",
                "userId": "mertagtas@gmail.com"
            },
            {
                "id": "f592d62c-8bed-404b-b726-83676c2ec0e0",
                "tagName": "stuff",
                "filePath": "/mertagtas",
                "fileName": "mertagtas.docx",
                "userId": "mertagtas@gmail.com"
            }
        ]


* DELETE : /api/1/tag/{tag_id}

    This rest api enables user to delete specific tag of a file with tag id which is obtained after adding tag.
    
    Header:
    
        Content-Type : application/json
        X-Auth-Access-Token : xxxxxxxxxxx
    
    Response Body : 
    
        {
            "message": "Operation is completed, successfully."
        }

* DELETE : /api/1/tag

    This rest api enables user to delete tags with tag name list.
    
    Header:
    
        Content-Type : application/json
        X-Auth-Access-Token : xxxxxxxxxxx
    
    Request Body :
    
        [
            "cool",
            "work"
        ]
        
    Response Body : 
    
        {
            "message": "Operation is completed, successfully."
        }

* POST : /api/1/files/search

    This rest api enables user to search files with specific tag list with just "OR" connection. !!! "AND" connection is not implemented !!!
       
    Header:
    
      Content-Type : application/json
      X-Auth-Access-Token : xxxxxxxxxxx
    
    Request Body :
    
         {
            "type": "OR",
            "maxResults": 10,
            "startIndex": 0,
            "tagNameList": [
              "cool",
              "stuff",
              "work"
            ]
          }
        
    Response Body : 
    
        [
            {
                "filePath": "/work",
                "fileName": "mertagtas.docx",
                "tagList":
                [
                    "stuff",
                    "work"
                ]
            },
            {
                "filePath": "/photos",
                "fileName": "holiday.png",
                "tagList":
                [
                    "cool"
                ]
            }
        ]


## ERROR HANDLING

   * All exception are catched by Spring Controller Advice class. After exception is catched, it returns the exception message to user with 409 header response.


   Response Body
      
      
        {
            "errorMessage":"XXXXXXXX"
        }
        

## CODE

Projects are consists of 5 packages which are
   - controller
        - This package contains the endpoint methods.
        
   - exception
        - This package includes project based exception. These exception are catched at exception 
        handler class.
        
   - filter
        - This package includes http filter beans such as AccessTokenAuthenticationFilter which 
        checks the authentication token for each request.
   - model
        - This package includes all model classes such as solr model and rest service request and 
        response model.
   
   - service
        - This package includes service beans such as dropbox client service which connects the 
        Dropbox API, solr client service which connects the Solr Instance

   - ApiExceptionHandler.java 
        - This class catches all exception that are thrown by rest api and returns appropiate 
        message.
   
   - SpringConfiguration.java 
		- configures external beans that project depends on 
		- and also manages the default values of 
			
			- solr.url 
			- dropbox.api.app.key : Dropbox App Key
			- dropbox.api.app.secret : Dropbox App Secret
			- dropbox.api.redirect.url : Dropbox Redirect URl after user confirmation 
    


