#!/usr/bin/env bash
echo "Creating docker_solr_1 docker container..."
docker run --name docker_solr_1 -d -p 8983:8983 -t solr 
sleep 5
echo "Creating solr core whose name is tags "
docker exec -it --user=solr docker_solr_1 solr create_core -c tags
echo "Copying managed schema to docker container"
docker cp managed-schema  docker_solr_1:/opt/solr/server/solr/tags/conf/managed-schema
echo "Restarting docker_solr_1 container"
docker stop docker_solr_1
docker start docker_solr_1
echo "Process is finished"