#!/usr/bin/env bash
cd ..
mvn clean
mvn package docker:build
docker run -p 8080:8080 -t mobilabsolutions/dropboxtagapi
