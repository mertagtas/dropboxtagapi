package com.mobilabsolutions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by mertagtas on 09/02/2017.
 */
@SpringBootApplication

public class DropboxTagApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DropboxTagApiApplication.class, args);
    }

}
