package com.mobilabsolutions;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.json.JsonReadException;
import com.dropbox.core.v2.DbxClientV2;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

/**
 * Created by mertagtas on 10/02/2017.
 */
@Configuration
public class SpringConfiguration {

    private final static Logger LOGGER = Logger.getLogger(SpringConfiguration.class);


    @Value("${solr.url}")
    private String solrUrl;

    @Value("${dropbox.api.app.key}")
    private String apiKey;

    @Value("${dropbox.api.app.secret}")
    private String appSecret;

    @Value("${dropbox.api.redirect.url}")
    private String redirectUrl;

    @Bean
    public SolrClient solrClient() {
        return new HttpSolrClient.Builder(getSolrUrl()).build();
    }

    @Bean(value = "dbxClientV2")
    @Scope("prototype")
    @Lazy(value = true)
    public DbxClientV2 getDbxClientV2(String accessToken) {
        DbxRequestConfig config = new DbxRequestConfig("dropbox/java-tutorial");
        return new DbxClientV2(config, accessToken);
    }

    @Bean
    public DbxAppInfo getDbxAppInfo() {
        try {
            // TODO : find a better way
            return DbxAppInfo.Reader.readFully("{\n" +
                    "  \"key\": \"" + getApiKey() + "\",\n" +
                    "  \"secret\": \"" + getAppSecret() + "\"\n" +
                    "}");
        } catch (JsonReadException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    // GETTERS AND SETTERS

    public String getSolrUrl() {
        String solrUrlProperty = System.getProperty("solr.url");
        if (!StringUtils.isEmpty(solrUrlProperty)) {
            solrUrl = solrUrlProperty;
        }
        return solrUrl;
    }

    public String getApiKey() {
        String dropboxApiAppKeyProperty = System.getProperty("dropbox.api.app.key");
        if (!StringUtils.isEmpty(dropboxApiAppKeyProperty)) {
            apiKey = dropboxApiAppKeyProperty;
        }
        return apiKey;
    }

    public String getAppSecret() {
        String dropboxApiAppSecretProperty = System.getProperty("dropbox.api.app.secret");
        if (!StringUtils.isEmpty(dropboxApiAppSecretProperty)) {
            appSecret = dropboxApiAppSecretProperty;
        }
        return appSecret;
    }

    public String getRedirectUrl() {
        String dropboxApiRedirectUrlProperty = System.getProperty("dropbox.api.redirect.url");
        if (!StringUtils.isEmpty(dropboxApiRedirectUrlProperty)) {
            redirectUrl = dropboxApiRedirectUrlProperty;
        }
        return redirectUrl;
    }
}
