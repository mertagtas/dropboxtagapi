package com.mobilabsolutions.dropboxtagapi.util;

import com.dropbox.core.v2.users.FullAccount;

import javax.servlet.http.HttpServletRequest;

import static com.mobilabsolutions.dropboxtagapi.CommonConstants.SESSION_ACOUNT;

/**
 * Created by mertagtas on 13/02/2017.
 */
public class UserUtil {

    // TODO : find better way such as : session controller

    public static String getAcountId(HttpServletRequest request) {
        FullAccount account = (FullAccount) request.getSession().getAttribute(SESSION_ACOUNT);
        return account.getEmail();
    }
}
