package com.mobilabsolutions.dropboxtagapi.filter;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.FullAccount;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobilabsolutions.dropboxtagapi.model.ErrorResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.mobilabsolutions.dropboxtagapi.CommonConstants.AUTH_TOKEN;
import static com.mobilabsolutions.dropboxtagapi.CommonConstants.SESSION_ACOUNT;
import static com.mobilabsolutions.dropboxtagapi.CommonConstants.SESSION_TOKEN;

/**
 * Created by mertagtas on 12/02/2017.
 */
@Component
public class AccessTokenAuthenticationFilter extends GenericFilterBean {

    private final static Logger LOGGER = Logger.getLogger(AccessTokenAuthenticationFilter.class);

    @Autowired
    private BeanFactory beanFactory;

    @Value("${server.context-path}")
    private String contextPath;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String url = httpServletRequest.getRequestURL().toString();
        if (!StringUtils.containsIgnoreCase(url, "/auth") && !StringUtils.endsWith(url, contextPath + "/")) {
            String accessToken = httpServletRequest.getHeader(AUTH_TOKEN);
            if (StringUtils.isEmpty(accessToken)) {
                finishRequest((HttpServletResponse) servletResponse, "Access Token is required.");
                return;
//                throw new AuthenticationException("Access token is required");
            }
            try {
                DbxClientV2 dbxClientV2 = beanFactory.getBean(DbxClientV2.class, accessToken);
                FullAccount account = dbxClientV2.users().getCurrentAccount();
                ((HttpServletRequest) servletRequest).getSession().setAttribute(SESSION_ACOUNT, account);
                ((HttpServletRequest) servletRequest).getSession().setAttribute(SESSION_TOKEN, accessToken);
            } catch (DbxException e) {
                LOGGER.error(e.getMessage(), e);
                finishRequest((HttpServletResponse) servletResponse, "Unauthorised request. Check your access token !!!");
                return;
//                throw new AuthenticationException("Authentication failed", e);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    // TODO : find better way such as redirecting API Exceptin Handler
    private void finishRequest(HttpServletResponse servletResponse, String message) {
        try {
            servletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            ErrorResponse errorResponse = new ErrorResponse(message);
            ObjectMapper mapper = new ObjectMapper();
            String errorResponseJson = mapper.writeValueAsString(errorResponse);
            servletResponse.getWriter().write(errorResponseJson);
            servletResponse.getWriter().flush();
            servletResponse.getWriter().close();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
