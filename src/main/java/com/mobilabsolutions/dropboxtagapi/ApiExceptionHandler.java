package com.mobilabsolutions.dropboxtagapi;

import com.mobilabsolutions.dropboxtagapi.exception.UnCheckedException;
import com.mobilabsolutions.dropboxtagapi.model.ErrorResponse;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 */
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private final static Logger LOGGER = Logger.getLogger(ApiExceptionHandler.class);

    @ExceptionHandler
    protected ResponseEntity<ErrorResponse> catchException(UnCheckedException e) {
        LOGGER.error(e.getMessage(), e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorResponse> catchException(Exception e) {
        LOGGER.error(e.getMessage(), e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.CONFLICT);
    }
}
