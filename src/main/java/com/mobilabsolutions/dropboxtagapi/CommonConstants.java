package com.mobilabsolutions.dropboxtagapi;

/**
 * Created by mertagtas on 12/02/2017.
 */
public class CommonConstants {
    public static final String SESSION_TOKEN = "token";
    public static final String SESSION_ACOUNT = "acount";
    public static final String AUTH_TOKEN = "X-Auth-Access-Token";

}
