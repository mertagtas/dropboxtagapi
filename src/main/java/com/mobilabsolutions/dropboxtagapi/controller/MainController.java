package com.mobilabsolutions.dropboxtagapi.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mertagtas on 11/02/2017.
 */
@RestController
public class MainController {

    @RequestMapping("/")
    public String home() {
        return "Welcome Dropbox Tag API";
    }

}
