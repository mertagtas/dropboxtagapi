package com.mobilabsolutions.dropboxtagapi.controller;

import com.dropbox.core.v2.users.FullAccount;
import com.mobilabsolutions.dropboxtagapi.exception.ApiValidationException;
import com.mobilabsolutions.dropboxtagapi.model.SuccessResponse;
import com.mobilabsolutions.dropboxtagapi.model.Tag;
import com.mobilabsolutions.dropboxtagapi.model.TagRequest;
import com.mobilabsolutions.dropboxtagapi.model.TagSearchRequest;
import com.mobilabsolutions.dropboxtagapi.service.TagService;
import com.mobilabsolutions.dropboxtagapi.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mobilabsolutions.dropboxtagapi.CommonConstants.SESSION_ACOUNT;

/**
 * Created by mertagtas on 09/02/2017.
 */
@RestController
@RequestMapping("/tag")
public class TagController {

//    private final static Logger LOGGER = Logger.getLogger(TagController.class);

    @Autowired
    private TagService tagService;

    @ResponseBody
    @RequestMapping(path = "", method = RequestMethod.POST)
    public List<Tag> addTag(@RequestBody TagRequest tagRequest, HttpServletRequest request) {
        if (StringUtils.isEmpty(tagRequest.getFileName())) {
            throw new ApiValidationException("File name is required");
        }
        if (StringUtils.isEmpty(tagRequest.getFilePath())) {
            throw new ApiValidationException("File path is required");
        }
        if (CollectionUtils.isEmpty(tagRequest.getTagList())) {
            throw new ApiValidationException("Tag List Required");
        }

        String userID = UserUtil.getAcountId(request);

        List<Tag> tagList = new ArrayList<>();
        for (String tagName : tagRequest.getTagList()) {
            if (!StringUtils.isAlphanumeric(tagName)) {
                throw new ApiValidationException(tagName + " should be alphanumeric string.");
            }
            tagList.add(new Tag(
                    tagName,
                    tagRequest.getFilePath(),
                    tagRequest.getFileName(),
                    userID)
            );
        }
        return tagService.save(tagList);
    }

    @ResponseBody
    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public SuccessResponse deleteTag(@PathVariable(name = "id") String id) {
        tagService.delete(id);
        return new SuccessResponse();
    }

    @ResponseBody
    @RequestMapping(path = "", method = RequestMethod.DELETE)
    public SuccessResponse deleteTag(@RequestBody List<String> tagNameList, HttpServletRequest request) {
        if (CollectionUtils.isEmpty(tagNameList)) {
            throw new ApiValidationException("Tag name list is required.");
        }
        tagService.deleteByTagNameList(UserUtil.getAcountId(request), tagNameList);
        return new SuccessResponse();
    }


}
