package com.mobilabsolutions.dropboxtagapi.controller;

import com.mobilabsolutions.dropboxtagapi.exception.ApiValidationException;
import com.mobilabsolutions.dropboxtagapi.model.*;
import com.mobilabsolutions.dropboxtagapi.service.DropboxApiClientService;
import com.mobilabsolutions.dropboxtagapi.service.TagService;
import com.mobilabsolutions.dropboxtagapi.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mobilabsolutions.dropboxtagapi.CommonConstants.SESSION_TOKEN;


/**
 * Created by mertagtas on 11/02/2017.
 */
@RestController
@RequestMapping("/files")
public class FileController {

    @Autowired
    private DropboxApiClientService dropboxApiClientService;

    @Autowired
    private TagService tagService;

    @RequestMapping(value = "/listFolder", method = RequestMethod.POST)
    @ResponseBody
    public List<DropboxFile> listFolder(@RequestBody FolderRequest folderRequest, HttpServletRequest httpServletRequest) {
        String token = (String) httpServletRequest.getSession().getAttribute(SESSION_TOKEN);
        return dropboxApiClientService.listFolder(token, folderRequest.getPath());
    }

    @ResponseBody
    @RequestMapping(path = "/search", method = RequestMethod.POST)
    public List<TagRequest> query(@RequestBody TagSearchRequest tagSearchRequest, HttpServletRequest request) {
        if (tagSearchRequest.getType() == null) {
            throw new ApiValidationException("Type is required.");
        }

        if (CollectionUtils.isEmpty(tagSearchRequest.getTagNameList())) {
            throw new ApiValidationException("Tag name or tag name list  is required.");
        }

        for (String tagName : tagSearchRequest.getTagNameList()) {
            if (!StringUtils.isAlphanumeric(tagName)) {
                throw new ApiValidationException(tagName + " should be alphanumeric string.");
            }
        }

        if (tagSearchRequest.getMaxResults() < 1) {
            throw new ApiValidationException("Max results should be bigger than 0.");
        }

        if (tagSearchRequest.getMaxResults() > 100) {
            throw new ApiValidationException("Max results should be lower than 100.");
        }

        if (tagSearchRequest.getStartIndex() < 0) {
            throw new ApiValidationException("Start index should be bigger than 0.");
        }

        String userID = UserUtil.getAcountId(request);

        List<Tag> tagList = tagService.query(tagSearchRequest, userID);
        List<TagRequest> tagSearchResponseList = new ArrayList<>();
        Map<String, TagRequest> filePathTagSearchResponseMap = new HashMap<>();
        for (Tag tag : tagList) {
            if (!filePathTagSearchResponseMap.containsKey(tag.getFilePath())) {
                TagRequest tagSearchResponse = new TagRequest();
                tagSearchResponse.setFileName(tag.getFileName());
                tagSearchResponse.setFilePath(tag.getFilePath());
                tagSearchResponseList.add(tagSearchResponse);
                filePathTagSearchResponseMap.put(tag.getFilePath(), tagSearchResponse);
            }
            TagRequest tagSearchResponse = filePathTagSearchResponseMap.get(tag.getFilePath());
            tagSearchResponse.getTagList().add(tag.getTagName());
        }
        return tagSearchResponseList;
    }

}
