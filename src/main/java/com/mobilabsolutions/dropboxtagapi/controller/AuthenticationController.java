package com.mobilabsolutions.dropboxtagapi.controller;

import com.dropbox.core.*;
import com.mobilabsolutions.SpringConfiguration;
import com.mobilabsolutions.dropboxtagapi.exception.AuthenticationException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * Created by mertagtas on 12/02/2017.
 */
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private final static Logger LOGGER = Logger.getLogger(AuthenticationController.class);

    @Autowired
    private DbxAppInfo appInfo;

    @Autowired
    private SpringConfiguration springConfiguration;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public void login(HttpServletRequest request, HttpServletResponse response) {
        try {
            DbxWebAuth.Request authRequest = DbxWebAuth.newRequestBuilder()
                    .withRedirectUri(springConfiguration.getRedirectUrl(), getSessionStore(request))
                    .build();

            String authorizeUrl = getWebAuth(request).authorize(authRequest);
            response.sendRedirect(authorizeUrl);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new AuthenticationException("API Key or API secret key is invalid!");
        }
    }


    @ResponseBody
    @RequestMapping(value = "/token", method = RequestMethod.GET)
    public DbxAuthFinish getToken(HttpServletRequest request, HttpServletResponse response) {
        try {
            return getWebAuth(request).finishFromRedirect(
                    springConfiguration.getRedirectUrl(),
                    getSessionStore(request),
                    request.getParameterMap()
            );
        } catch (DbxException | DbxWebAuth.BadRequestException
                | DbxWebAuth.ProviderException | DbxWebAuth.NotApprovedException
                | DbxWebAuth.BadStateException | DbxWebAuth.CsrfException e) {
            LOGGER.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    private DbxSessionStore getSessionStore(final HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        String sessionKey = "dropbox-auth-csrf-token";
        return new DbxStandardSessionStore(session, sessionKey);
    }

    private DbxWebAuth getWebAuth(final HttpServletRequest request) {
        return new DbxWebAuth(getRequestConfig(request), appInfo);
    }

    private DbxRequestConfig getRequestConfig(HttpServletRequest request) {
        return DbxRequestConfig.newBuilder("tag-api")
                .withUserLocaleFrom(request.getLocale())
                .build();
    }

}
