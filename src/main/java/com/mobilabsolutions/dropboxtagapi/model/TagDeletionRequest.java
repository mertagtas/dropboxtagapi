package com.mobilabsolutions.dropboxtagapi.model;

import java.util.List;

/**
 * Created by mertagtas on 11/02/2017.
 */
public class TagDeletionRequest {

    private List<String> tagNameList;

    // GETTERS AND SETTERS

    public List<String> getTagNameList() {
        return tagNameList;
    }

    public void setTagNameList(List<String> tagNameList) {
        this.tagNameList = tagNameList;
    }

}
