package com.mobilabsolutions.dropboxtagapi.model;

/**
 * Created by mertagtas on 12/02/2017.
 */
public class SuccessResponse {
    private String message = "Operation is completed, successfully.";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
