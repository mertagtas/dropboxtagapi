package com.mobilabsolutions.dropboxtagapi.model;

/**
 * Created by mertagtas on 11/02/2017.
 */
public class DropboxFile {

    private String tag;
    private String path;
    private String name;

    public DropboxFile(String path, String name, String tag) {
        this.path = path;
        this.name = name;
        this.tag = tag;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
