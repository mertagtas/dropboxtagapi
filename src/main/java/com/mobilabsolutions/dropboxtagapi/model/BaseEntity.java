package com.mobilabsolutions.dropboxtagapi.model;

import com.mobilabsolutions.dropboxtagapi.annotation.SolrItem;

/**
 * Created by mertagtas on 10/02/2017.
 */
public class BaseEntity {

    protected String id;

    @SolrItem(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
