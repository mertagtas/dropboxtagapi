package com.mobilabsolutions.dropboxtagapi.model;

/**
 * Created by mertagtas on 11/02/2017.
 */
public class FolderRequest {
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
