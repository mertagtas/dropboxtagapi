package com.mobilabsolutions.dropboxtagapi.model;

import org.apache.solr.client.solrj.SolrQuery;

import java.util.List;

/**
 * Created by mertagtas on 12/02/2017.
 */
public class TagSearchRequest {
    private QueryType type = QueryType.OR;
    private List<String> tagNameList;
    private int maxResults = 10;
    private int startIndex = 0;

    public SolrQuery buildSolrQuery(String userId) {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setRequestHandler("/select");
        solrQuery.setParam("start", String.valueOf(startIndex));
        solrQuery.setParam("rows", String.valueOf(maxResults));
        solrQuery.setParam("fl", "*");
        solrQuery.addSort(Tag.FILE_PATH_PROPERTY, SolrQuery.ORDER.asc);
        solrQuery.addFilterQuery(Tag.USER_ID_PROPERTY + ":" + userId);

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < tagNameList.size(); i++) {
            stringBuilder.append(" ").append(Tag.TAG_NAME_PROPERTY).append(":").append(tagNameList.get(i));
            if (i != tagNameList.size() - 1) {
                stringBuilder.append(" ").append(type.name());
            }
        }
        solrQuery.setQuery(stringBuilder.toString());
        return solrQuery;
    }

    public QueryType getType() {
        return type;
    }

    public void setType(QueryType type) {
        this.type = type;
    }

    public List<String> getTagNameList() {
        return tagNameList;
    }

    public void setTagNameList(List<String> tagNameList) {
        this.tagNameList = tagNameList;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

}
