package com.mobilabsolutions.dropboxtagapi.model;

import com.mobilabsolutions.dropboxtagapi.annotation.SolrItem;

/**
 * Created by mertagtas on 10/02/2017.
 */
public class Tag extends BaseEntity {

    public static final String TAG_NAME_PROPERTY = "tagName";
    public static final String FILE_PATH_PROPERTY = "filePath";
    public static final String FILE_NAME_PROPERTY = "fileName";
    public static final String USER_ID_PROPERTY = "userId";

    private String tagName;
    private String filePath;
    private String fileName;
    private String userId;

    // CONSTUCTOR

    public Tag() {
    }

    public Tag(String id) {
        this.id = id;
    }

    public Tag(String tagName, String filePath, String fileName, String userId) {
        this.tagName = tagName;
        this.filePath = filePath;
        this.fileName = fileName;
        this.userId = userId;
    }

    // GETTERS AND SETTERS

    @SolrItem(name = TAG_NAME_PROPERTY)
    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @SolrItem(name = FILE_PATH_PROPERTY)
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @SolrItem(name = FILE_NAME_PROPERTY)
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @SolrItem(name = USER_ID_PROPERTY)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
