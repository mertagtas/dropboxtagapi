package com.mobilabsolutions.dropboxtagapi.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mertagtas on 11/02/2017.
 */
public class TagRequest {

    private String filePath;
    private String fileName;
    private List<String> tagList = new ArrayList<>();

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<String> getTagList() {
        return tagList;
    }

    public void setTagList(List<String> tagList) {
        this.tagList = tagList;
    }

}
