package com.mobilabsolutions.dropboxtagapi.model;

/**
 * Created by mertagtas on 12/02/2017.
 */
public enum QueryType {
    AND, OR;
}
