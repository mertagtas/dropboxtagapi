package com.mobilabsolutions.dropboxtagapi.service;

import com.mobilabsolutions.dropboxtagapi.model.DropboxFile;

import java.util.List;

/**
 * Created by mertagtas on 11/02/2017.
 */
public interface DropboxApiClientService {

    List<DropboxFile> listFolder(String accessToken, String rootFolder);
}
