package com.mobilabsolutions.dropboxtagapi.service;

import com.mobilabsolutions.dropboxtagapi.model.Tag;
import com.mobilabsolutions.dropboxtagapi.model.TagSearchRequest;

import java.util.List;

/**
 * Created by mertagtas on 09/02/2017.
 */
public interface TagService {

    List<Tag> save(List<Tag> tagList);

    List<Tag> query(TagSearchRequest tagSearchRequest, String accountId);

    void delete(String id);

    void deleteByTagNameList(String userId, List<String> tagNameList);

}
