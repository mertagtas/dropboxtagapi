package com.mobilabsolutions.dropboxtagapi.service;

import com.mobilabsolutions.dropboxtagapi.exception.SolrDeletionException;
import com.mobilabsolutions.dropboxtagapi.model.Tag;
import com.mobilabsolutions.dropboxtagapi.model.TagSearchRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by mertagtas on 10/02/2017.
 */
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private SolrClientService solrClientService;

    @Override
    public List<Tag> save(List<Tag> tagList) {
        for (Tag tag : tagList) {
            tag.setId(UUID.randomUUID().toString());
        }
        solrClientService.save(tagList);
        solrClientService.commit();
        return tagList;
    }

    @Override
    public List<Tag> query(TagSearchRequest tagSearchRequest, String accountId) {
        QueryResponse query = solrClientService.query(tagSearchRequest.buildSolrQuery(accountId));
        SolrDocumentList results = query.getResults();
        List<Tag> tagList = new ArrayList<>();
        for (SolrDocument result : results) {
            String fileName = (String) result.get(Tag.FILE_NAME_PROPERTY);
            String filePath = (String) result.get(Tag.FILE_PATH_PROPERTY);
            String tagName = (String) result.get(Tag.TAG_NAME_PROPERTY);
            String userId = (String) result.get(Tag.USER_ID_PROPERTY);
            tagList.add(new Tag(tagName, filePath, fileName, userId));
        }
        return tagList;
    }

    @Override
    public void delete(String id) {
        solrClientService.delete(id);
        solrClientService.commit();
    }

    @Override
    public void deleteByTagNameList(String userId, List<String> tagNameList) {
        for (String value : tagNameList) {
            try {
                String query = Tag.USER_ID_PROPERTY + ":" + userId + " AND " + Tag.TAG_NAME_PROPERTY + ":" + URLEncoder.encode(value, "UTF-8");
                solrClientService.deleteByQuery(query);
            } catch (UnsupportedEncodingException e) {
                throw new SolrDeletionException(e.getMessage(), e);
            }
        }
        solrClientService.commit();

    }

}
