package com.mobilabsolutions.dropboxtagapi.service;

import com.mobilabsolutions.dropboxtagapi.annotation.SolrItem;
import com.mobilabsolutions.dropboxtagapi.exception.*;
import com.mobilabsolutions.dropboxtagapi.model.BaseEntity;
import com.mobilabsolutions.dropboxtagapi.model.Tag;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by mertagtas on 10/02/2017.
 */
@Service
public class SolrClientServiceImpl implements SolrClientService {

    private final static Logger LOGGER = Logger.getLogger(SolrClientServiceImpl.class);

    @Autowired
    private SolrClient solrClient;

    @Override
    public UpdateResponse save(BaseEntity baseEntity) {
        SolrInputDocument solrInputDocument = convertToDocument(baseEntity);
        try {
            return solrClient.add(solrInputDocument);
        } catch (SolrServerException | IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SolrAdditonException(e.getMessage(), e);
        }
    }

    @Override
    public <T extends BaseEntity> void save(List<T> entityList) {
        try {
            for (T entity : entityList) {
                solrClient.add(convertToDocument(entity));
            }
        } catch (SolrServerException | IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SolrAdditonException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(String id) {
        try {
            solrClient.deleteById(id);
        } catch (SolrServerException | IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SolrDeletionException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteByQuery(String query) {
        try {
            solrClient.deleteByQuery(query);
        } catch (SolrServerException | IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SolrDeletionException(e.getMessage(), e);
        }
    }

    @Override
    public QueryResponse query(SolrQuery solrQuery) {
        try {
            return solrClient.query(solrQuery);
        } catch (SolrServerException | IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SolrQueryException(e.getMessage(), e);
        }
    }

    @Override
    public void commit() {
        try {
            solrClient.commit();
        } catch (SolrServerException | IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SolrCommitException(e.getMessage(), e);
        }
    }

    private SolrInputDocument convertToDocument(BaseEntity baseEntity) {
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        for (Method m : baseEntity.getClass().getMethods()) {
            SolrItem mXY = m.getAnnotation(SolrItem.class);
            if (mXY != null) {
                try {
                    String name = mXY.name();
                    Object value = m.invoke(baseEntity);
                    solrInputDocument.addField(name, value);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw new SolrConversionException(e.getMessage(), e);
                }
            }
        }
        return solrInputDocument;
    }

  /*  public static void main(String... args) {
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        solrInputDocument.addField(Tag.FILE_NAME_PROPERTY, "1");
        solrInputDocument.addField(Tag.FILE_PATH_PROPERTY, "1");
        solrInputDocument.addField(Tag.USER_ID_PROPERTY, "1");
        solrInputDocument.addField(Tag.TAG_NAME_PROPERTY, "1");
        solrInputDocument.addField("id", "1");

        HttpSolrClient solrClient = new HttpSolrClient.Builder("http://localhost:8983/solr/tags").build();

        try {
            solrClient.add(solrInputDocument);
            solrClient.commit();
        } catch (SolrServerException | IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SolrAdditonException(e.getMessage(), e);
        }

    }*/

}
