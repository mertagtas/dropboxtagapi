package com.mobilabsolutions.dropboxtagapi.service;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.mobilabsolutions.dropboxtagapi.exception.DropboxClientException;
import com.mobilabsolutions.dropboxtagapi.model.DropboxFile;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mertagtas on 11/02/2017.
 */
@Service
public class DropboxApiClientServiceImpl implements DropboxApiClientService {

    private final static Logger LOGGER = Logger.getLogger(DropboxApiClientServiceImpl.class);

    @Autowired
    private BeanFactory beanFactory;

    @Override
    public List<DropboxFile> listFolder(String accessToken, String rootFolder) {
        DbxClientV2 dbxClientV2 = beanFactory.getBean(DbxClientV2.class, accessToken);
        try {
            List<DropboxFile> fileList = new ArrayList<>();
            ListFolderResult result = dbxClientV2.files().listFolder(rootFolder);
            while (true) {
                for (Metadata metadata : result.getEntries()) {
                    fileList.add(new DropboxFile(
                            metadata.getPathLower(),
                            metadata.getName(),
                            metadata instanceof FolderMetadata ? "folder" : "file"));
                }
                if (!result.getHasMore()) {
                    break;
                }
                result = dbxClientV2.files().listFolderContinue(result.getCursor());
            }
            return fileList;
        } catch (DbxException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DropboxClientException(e.getMessage(), e);
        }
    }

}
