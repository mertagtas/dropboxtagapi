package com.mobilabsolutions.dropboxtagapi.service;

import com.mobilabsolutions.dropboxtagapi.model.BaseEntity;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;

import java.util.List;

/**
 * Created by mertagtas on 10/02/2017.
 */

public interface SolrClientService {

    UpdateResponse save(BaseEntity baseEntity);

    <T extends BaseEntity> void save(List<T> entityList);

    void delete(String id);

    void deleteByQuery(String query);

    QueryResponse query(SolrQuery solrQuery);

    void commit();
}
