package com.mobilabsolutions.dropboxtagapi.exception;

/**
 * Created by mertagtas on 12/02/2017.
 */
public class AuthenticationException extends UnCheckedException {
    public AuthenticationException() {
    }

    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthenticationException(Throwable cause) {
        super(cause);
    }
}
