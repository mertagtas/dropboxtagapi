package com.mobilabsolutions.dropboxtagapi.exception;

/**
 * Created by mertagtas on 13/02/2017.
 */
public class SolrCommitException extends UnCheckedException {

    public SolrCommitException() {
        super();
    }

    public SolrCommitException(String message) {
        super(message);
    }

    public SolrCommitException(String message, Throwable cause) {
        super(message, cause);
    }

    public SolrCommitException(Throwable cause) {
        super(cause);
    }
}
