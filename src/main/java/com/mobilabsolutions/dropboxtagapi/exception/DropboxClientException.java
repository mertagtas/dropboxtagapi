package com.mobilabsolutions.dropboxtagapi.exception;

/**
 * Created by mertagtas on 11/02/2017.
 */
public class DropboxClientException extends UnCheckedException {

    public DropboxClientException() {
    }

    public DropboxClientException(String message) {
        super(message);
    }

    public DropboxClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public DropboxClientException(Throwable cause) {
        super(cause);
    }
}

