package com.mobilabsolutions.dropboxtagapi.exception;

/**
 * Created by mertagtas on 11/02/2017.
 */
public class UnCheckedException extends RuntimeException {

    public UnCheckedException() {
    }

    public UnCheckedException(String message) {
        super(message);
    }

    public UnCheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnCheckedException(Throwable cause) {
        super(cause);
    }
}
