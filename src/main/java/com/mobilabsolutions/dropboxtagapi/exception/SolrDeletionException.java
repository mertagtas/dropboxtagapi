package com.mobilabsolutions.dropboxtagapi.exception;

/**
 * Created by mertagtas on 09/02/2017.
 */
public class SolrDeletionException extends RuntimeException {

    public SolrDeletionException() {
    }

    public SolrDeletionException(String message) {
        super(message);
    }

    public SolrDeletionException(String message, Throwable cause) {
        super(message, cause);
    }

    public SolrDeletionException(Throwable cause) {
        super(cause);
    }

}
