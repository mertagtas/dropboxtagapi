package com.mobilabsolutions.dropboxtagapi.exception;

/**
 * Created by mertagtas on 09/02/2017.
 */
public class SolrQueryException extends RuntimeException {

    public SolrQueryException() {
    }

    public SolrQueryException(String message) {
        super(message);
    }

    public SolrQueryException(String message, Throwable cause) {
        super(message, cause);
    }

    public SolrQueryException(Throwable cause) {
        super(cause);
    }

}
