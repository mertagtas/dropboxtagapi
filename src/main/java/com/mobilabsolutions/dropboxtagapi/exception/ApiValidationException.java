package com.mobilabsolutions.dropboxtagapi.exception;

/**
 * Created by mertagtas on 11/02/2017.
 */
public class ApiValidationException extends UnCheckedException {

    public ApiValidationException() {
    }

    public ApiValidationException(String message) {
        super(message);
    }

    public ApiValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiValidationException(Throwable cause) {
        super(cause);
    }
}
