package com.mobilabsolutions.dropboxtagapi.exception;

/**
 * Created by mertagtas on 09/02/2017.
 */
public class SolrConversionException extends RuntimeException {

    public SolrConversionException() {
    }

    public SolrConversionException(String message) {
        super(message);
    }

    public SolrConversionException(String message, Throwable cause) {
        super(message, cause);
    }

    public SolrConversionException(Throwable cause) {
        super(cause);
    }

}
