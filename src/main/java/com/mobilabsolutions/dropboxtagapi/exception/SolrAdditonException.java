package com.mobilabsolutions.dropboxtagapi.exception;

/**
 * Created by mertagtas on 09/02/2017.
 */
public class SolrAdditonException extends RuntimeException {

    public SolrAdditonException() {
    }

    public SolrAdditonException(String message) {
        super(message);
    }

    public SolrAdditonException(String message, Throwable cause) {
        super(message, cause);
    }

    public SolrAdditonException(Throwable cause) {
        super(cause);
    }

}
